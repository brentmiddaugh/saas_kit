require 'test_helper'

class RegistrationsControllerTest < ActionDispatch::IntegrationTest

  describe '#new' do
    before do
      get '/try'
    end

    it 'should get new' do
      assert_response :success
    end

    it 'should have a form' do
      assert_select 'form'
    end

    it 'should have a company name field' do
      assert_select 'input#registration_company_name'
    end

    it 'should have a user email field' do
      assert_select 'input#registration_user_email'
    end

    it 'should have a user password field' do
      assert_select 'input#registration_user_password'
    end

    it 'should have a user password confirmation field' do
      assert_select 'input#registration_user_password_confirmation'
    end

    it 'should have a submit button' do
      assert_select "input[type='submit']"
    end
  end

  describe '#create' do
    let(:params) {
      { registration: { company_name: 'Test Company',
                        user_email: 'jill@example.dev',
                        user_password: 'password',
                        user_password_confirmation: 'password'}}
    }

    it 'should create a company' do
      assert_difference('Company.count') do
        post '/try', params: params
      end
    end

    it 'should create a user' do
      assert_difference('User.count') do
        post '/try', params: params
      end
    end

    it 'should redirect' do
      post '/try', params: params

      assert_response :redirect
      assert_redirected_to app_root_path
    end

  end

end
