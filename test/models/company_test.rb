require 'test_helper'

class CompanyTest < ActiveSupport::TestCase
  let(:subject) { companies(:one)}

  it 'should be valid' do
    subject.valid?.must_equal true
  end

  it 'should have a name' do
    subject.name = nil
    subject.valid?.must_equal false
  end
end
