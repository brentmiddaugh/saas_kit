class AddPlanIdToCompanies < ActiveRecord::Migration[5.1]
  def change
    add_column :companies, :plan_id, :integer
  end
end
