Rails.application.routes.draw do

  namespace :app do
    get '/', to: 'home#index', as: :root
  end

  get 'try', to: 'marketing/registrations#new'
  post 'try', to: 'marketing/registrations#create'


  devise_for :users

  root 'marketing/landing#index'
end
