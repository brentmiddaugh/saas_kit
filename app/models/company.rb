class Company < ApplicationRecord
  enum status: [ :trial, :trial_ended, :active, :suspended, :closed]

  validates :name, presence: true
  validates :plan_id, presence: true
end
