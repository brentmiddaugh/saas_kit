class RegistrationForm
  include ActiveModel::Model

  attr_accessor :company_name, :user_email, :user_password, :user_password_confirmation, :plan_id

  def self.model_name
    ActiveModel::Name.new(self, nil, 'Registration')
  end

  def plans
    [OpenStruct.new(id: 1, name: 'Start Up'),
     OpenStruct.new(id: 2, name: 'Small Business'),
     OpenStruct.new(id: 3, name: 'Enterprise')]
  end

  def save
    if valid?
      Company.transaction do
        company = Company.new(name: company_name, plan_id: plan_id)
        user    = User.new(company: company,
                           email: user_email,
                           password: user_password,
                           password_confirmation: user_password_confirmation)

        if company.valid? && user.valid?
          company.save
          user.save
          true
        else
          company.errors.each do |attribute, error|
            errors.add("company_#{attribute}".to_sym, error)
          end
          user.errors.each do |attribute, error|
            errors.add("user_#{attribute}".to_sym, error)
          end
          false
        end
      end
    else
      false
    end
  end

end
