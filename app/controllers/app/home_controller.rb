class App::HomeController < ApplicationController

  def index
    @page_title = "#{current_user.company.name} - Home"
  end

end
