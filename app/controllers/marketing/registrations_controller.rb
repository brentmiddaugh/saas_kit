class Marketing::RegistrationsController < ApplicationController
  respond_to :html

  def new
    @form = RegistrationForm.new
  end

  def create
    @form = RegistrationForm.new(create_params)

    if @form.save
      redirect_to app_root_path
    else
      respond_with @form, location: '/try'
    end

  end

  private
  def create_params
    params
      .require(:registration)
      .permit(:company_name,
              :plan_id,
              :user_email,
              :user_password,
              :user_password_confirmation)
  end

end
